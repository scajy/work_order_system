import store from "@/store";

export default {
  mounted(el, bindings) {
    let allowRoleList = bindings.value;
    let userRole = store.state.role;

    // 无权限 则不加载
    if (allowRoleList.indexOf(userRole) === -1) {
      // 删除vue里的元素
      el.parentNode && el.parentNode.removeChild(el);
    }
  },
};

