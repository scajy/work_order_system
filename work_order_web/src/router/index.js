import { createRouter, createWebHistory } from 'vue-router'
import Layout from '../layout/index.vue'
import Login from '../Login.vue'
import store from "@/store";

const routes = [
  {
    path: '/login',
    name: '登录',
    component: Login,
  },
  {
    path: '/',
    name: '首页',
    component: Layout,
    redirect: '/dashboard', // 重定向跳转到仪表盘
    children: [
      {
        path: '/dashboard',
        name: '仪表盘',
        icon: "HomeFilled",
        component: () => import('../views/dashboard/index.vue'),
        // 定义权限以admin管理员允许访问，并加载
        meta: { role: ["admin", "user"], title: "仪表盘", is_menu: true  },
      }
    ]
  },
  {
    path: '/work_order',
    name: '工单系统',
    component: Layout,
    icon: "Menu",
    children: [
      {
        path: '/work_order/department',
        name: '部门管理',
        component: () => import('../views/department/index.vue'),
        // 定义权限以admin管理员允许访问，并加载
        meta: { role: ["admin", "user"], title: "部门管理", is_menu: true  },
      },
      {
        path: '/work_order/project',
        name: '项目管理',
        component: () => import('../views/project/index.vue'),
        // 定义权限以admin管理员允许访问，并加载
        meta: { role: ["admin", "user"], title: "项目管理", is_menu: true  },
      },
      {
        path: '/work_order/server',
        name: '服务管理',
        component: () => import('../views/server/index.vue'),
        // 定义权限以admin管理员允许访问，并加载
        meta: { role: ["admin", "user"], title: "服务管理", is_menu: true  },
      },
      {
        path: '/work_order/version',
        name: '版本管理',
        component: () => import('../views/master_version/index.vue'),
        // 定义权限以admin管理员允许访问，并加载
        meta: { role: ["admin", "user"], title: "版本管理", is_menu: true  },
      },
      {
        path: '/work_order/all',
        name: '工单系统',
        component: () => import('../views/work_order/index.vue'),
        // 定义权限以admin管理员允许访问，并加载
        meta: { role: ["admin", "user"], title: "工单系统", is_menu: true  },
      },
      {
        path: '/work_order/my_work_order',
        name: '我的工单',
        component: () => import('../views/my_work_order/index.vue'),
        // 定义权限以admin管理员允许访问，并加载
        meta: { role: ["admin", "user"], title: "我的工单系统", is_menu: true  },
      },
    ]
  },
  {
    path: '/access_control',
    name: '权限控制',
    component: Layout,
    icon: "Setting",
    children: [
      {
        path: '/access_control/users',
        name: '用户管理',
        component: () => import('../views/users/index.vue'),
        // 定义权限以admin管理员允许访问，并加载
        meta: { role: ["admin",], title: "用户管理", is_menu: true  },
      },
      {
        path: '/access_control/permissions',
        name: '权限管理',
        component: () => import('../views/permissions/index.vue'),
        // 定义权限以admin管理员允许访问，并加载
        meta: { role: ["admin",], title: "权限管理", is_menu: true  },
      },
      {
        path: '/access_control/roles',
        name: '角色管理',
        component: () => import('../views/roles/index.vue'),
        // 定义权限以admin管理员允许访问，并加载
        meta: { role: ["admin",], title: "角色管理", is_menu: true  },
      },
    ]
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

// 为路由对象，添加 beforeEach 导航守卫
router.beforeEach((to, form, next) => {
  /*
   to 将要访问的路径
   from 代表从那个路径跳转而来
   next 是一个函数，表示放行
      next() 放行   next('/login') 强制跳转
  */
  // 从 sessionStorage 中获取到 保存的 token 值
  const tokenStr = window.localStorage.getItem('token')
  // 1.当前访问的路由权限
  // console.log(to.meta.role);
  
  // 2. 如果用户访问的登录页，直接放行
  if (to.path === '/login') return next()

  // 3. 没有token，强制跳转到登录页
  if (!tokenStr) {
    localStorage.clear();
    return next('/login')
  }

  // 4. 我们通过判断当前访问路由角色，没有就放行，有就进入下一层进行判断
  if (to.meta.role) {
    // 1. 获取当前vuex中自己的角色
    let userRole = store.state.role;
    let allowRoleList = to.meta.role;

    // 2. to.meta.role就是允许的角色列表 ["admin", "user"]
    // 根据返回值索引，不存在就返回的-1，存在就会大于-1,没有权限就跳回登陆界面，有权限继续放行
    if (allowRoleList.indexOf(userRole) === -1) {
      localStorage.clear();
      next({ path: "login" });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router
