import { createStore } from "vuex";

export default createStore({
  state: {
    token: localStorage.getItem("token") || "",
    role:  localStorage.getItem("roles") || "",
  },
  getters: {},
  mutations: {
    login(state, {token, role}) {
      state.token = token;
      state.role = role;
    }
  },
  actions: {},
  modules: {},
});

