import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

// 导入elementui和css
import Elementplus from "element-plus";
import "element-plus/dist/index.css";
// 添加为中文显示
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'

// 注册icon图标
import * as ElementPlusIconsVue from '@element-plus/icons-vue';

// 导入axios封装模块
import axios from './api/http'

// 导入自定义权限组件 
import directives from "./directives";

// 创建实例
const app = createApp(App);

// 统一注册el-icon图标
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
// axios全局注册
app.config.globalProperties.$http = axios;

// 注册store
app.use(store);
// 注册router
app.use(router);
// 注册Elementplus
app.use(Elementplus,{locale: zhCn,});
// 注册自定义权限组件
app.use(directives);
// 挂载实例
app.mount("#app");
