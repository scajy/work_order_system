# 线上部署-后端



### 1. 线上部署-后端

- 部署架构

  Nginx 前端Web服务，接收到动态请求通过uwsgi模块将请求转发给uwsgi服务器，uwsgi服务器通过django处理完后返回给Nginx，Nginx返回用户浏览器展示。

   

  **既然uwsgi是一个可以独立部署的服务器，为什么还用Nginx代理？**

  •     Nginx作为入口可配置安全策略，并且可以为uwsgi提供负载均衡。

  •     Nginx处理静态资源能力强

- 将本地项目开发的项目环境打包

  - 导出依赖模块列表

    ```shell
    (cmdb) [admin@shichaodeMacBook-Pro cmdb_BE]#  pip3 freeze > requirements.txt
    
    (cmdb) [admin@shichaodeMacBook-Pro cmdb_BE]#  cat requirements.txt 
    asgiref==3.7.2
    Django==3.2.13
    django-cors-headers==4.2.0
    django-filter==23.2
    djangorestframework==3.14.0
    numpy==1.24.4
    pandas==2.0.3
    PyMySQL==1.1.0
    python-dateutil==2.8.2
    pytz==2023.3
    six==1.16.0
    sqlparse==0.4.4
    typing_extensions==4.7.1
    tzdata==2023.3
    ```
    
  - 修改数据库驱动或配置
  
    ```python
    # devops_BE/__init__.py
    import pymysql
    pymysql.install_as_MySQLdb()
    
    ```
  
    ```python
    # vi devops_BE/settings.py
    
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'work_order',
            'USER': 'root',
            'PASSWORD': '123456',
            'HOST': '127.0.0.1',
            'PORT': 3306,
        }
    }
    ```
  
  - 关闭debug模式和白名单：
  
    ```python
    DEBUG = False   # 调试模式
    ALLOWED_HOSTS = ['*']  # 白名单，只允许列表中的ip访问，*代表所有
    ```
  
- 服务器环境准备

  - 安装python3

    ```shell
    yum install zlib-devel libffi-devel mysql-devel bzip2-devel git -y
    wget  https://www.python.org/ftp/python/3.8.4/Python-3.8.4.tgz
    tar zxvf Python-3.8.4.tgz
    cd Python-3.8.4
    ./configure 
    make && make install
    ```

  - 安装依赖模块列表

    ```
    pip3 install -r requirements.txt -i https://mirrors.aliyun.com/pypi/simple
    ```

  - 安装数据库

    ```shell
    docker run -d --name db -p 3306:3306 -v mysqldata:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 mysql:5.7 --character-set-server=utf8
    
    docker exec -it db bash
    root@e2eff2d75dd2:/# mysql -uroot -p$MYSQL_ROOT_PASSWORD -e "create database cmdb;"
    
    ```

  - 启动开发环境，验证依赖模块

    ```shell
    python3 manage.py runserver 0.0.0.0:8080
    ```

  - 测试问题，同步数据库

    ```shell
    python3 manage.py migrate
    ```

  - 收集静态文件

    ```python
    # 编写settings.py文件，创建生成静态文件目录
    vi settings.py
    STATIC_URL = '/static/'
    STATIC_ROOT = os.path.join(BASE_DIR,'static')
    
    # 执行生成静态
    python3 manage.py collectstatic
    
    ```

  - 创建管理员账号：

    ```
    python3 manage.py createsuperuser
    ```
- **使用daphne运行django项目**

  - daphne是一个[Web服务器](https://baike.baidu.com/item/Web服务器/8390210)，也是Python的一个模块，直接pip安装即可： 

    ```
    pip3 install daphne -i https://mirrors.aliyun.com/pypi/simple
    ```

  - **切换目录**

    ```shell
    # cd "项目目录"
    [root@cmdb ~]# cd /opt/cmdb_BE/
    [root@cmdb cmdb_BE]# ll
    总用量 20
    drwxr-xr-x 5 root root  191 7月  14 16:08 cmdb
    drwxr-xr-x 3 root root  108 7月  14 16:08 cmdb_BE
    drwxr-xr-x 4 root root  179 7月  14 16:08 domain
    drwxr-xr-x 4 root root 4096 7月  14 16:08 libs
    -rwxr-xr-x 1 root root  663 7月  14 16:05 manage.py
    -rw-r--r-- 1 root root 5746 7月  14 16:05 README.md
    -rw-r--r-- 1 root root 1097 7月  14 16:05 requirements.txt
    drwxr-xr-x 4 root root  179 7月  14 16:08 system_config
    
    ```

  - 手动启动服务测试

    ```
    daphne -b 0.0.0.0 -p8080 --websocket_timeout 300 cmdb_BE.asgi:application
    ```

  - 编写daphne有系统服务启动

    ```shell
    [root@cmdb cmdb_BE]# vim /etc/systemd/system/daphne.service
    [Unit]
    Description=Daphne ASGI Server
    After=network.target
    
    [Service]
    Type=simple
    WorkingDirectory=/opt/cmdb_BE
    ExecStart=/usr/local/bin/daphne -b 0.0.0.0 -p8080 --websocket_timeout 300  cmdb_BE.asgi:application
    ExecStop=/bin/kill $MAINPID
    Restart=always
    
    [Install]
    WantedBy=multi-user.target
    ```

    > WorkingDirectory : 是程序工作目录，请自行修改

  - 启动重启和状态查看

    ```
    重新加载配置：systemctl daemon-reload
    启动服务：sudo systemctl start daphne
    停止服务：sudo systemctl stop daphne
    重新启动服务：sudo systemctl restart daphne
    启用服务（开机启动）：sudo systemctl enable daphne
    ```

  - daphne命令详解

    - **--port:** 指定 Daphne 监听的端口，默认为8000。
    - **--bind:** 指定 Daphne 监听的 IP 地址和端口，格式为ip:port。
    - **--unix-socket:** 指定 Daphne 监听的 Unix 域套接字路径。
    - **--verbosity:** 指定日志详细程度，可选的值有 0（只输出错误信息）、1（输出错误信息和警告信息）、2（输出错误信息、警告信息和一般信息）。
    - **--access-log:** 指定访问日志文件路径，如果不指定，则不会记录访问日志。
    - **--root-path:** 指定静态文件根目录，用于提供静态文件服务。
    - **--ping-interval:** 指定 Websocket ping 的间隔时间，单位为秒，默认为20秒。
    - **--ping-timeout:** 指定 Websocket ping 超时时间，单位为秒，默认为30秒。
    - **--http-timeout:** 指定 HTTP 请求超时时间，单位为秒，默认为120秒。
    - **--websocket-timeout:** 指定 Websocket 连接超时时间，单位为秒，默认为120秒。


- 配置nginx

  - 安装nginx服务

    ```shell
    yum install epel-release –y
    yum install nginx -y
    ```

  - 配置nginx配置文件

    ```shell
    [root@prod conf.d]# cat api.scajy.cn.conf 
    server {
            listen       80;
            server_name  api.scajy.cn;
    
            # 请求webssh
            location ~ / {
                proxy_pass http://127.0.0.1:8080;
                proxy_read_timeout 60s;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection 'Upgrade';
            }
            # 静态文件目录
            location /static {
               alias /opt/devops_api/static;
            }
    }
    ```

  - 重启nginx

    ```shell
    [root@prod conf.d]# nginx -t
    nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
    nginx: configuration file /etc/nginx/nginx.conf test is successful
    [root@prod conf.d]# nginx -s reload
    ```

  

- 浏览器验证api接口是否正常

  
