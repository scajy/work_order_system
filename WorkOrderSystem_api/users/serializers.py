# 导入数据模型
from .models import User, Permissions, Roles

# 导入虚拟化工具
from rest_framework import serializers


class PermissionsSerializer(serializers.ModelSerializer):
    """
    权限序列化类
    """
    class Meta:
        model = Permissions
        fields = "__all__"
        read_only_fields = ("id",)   # 仅用于序列化（只读）字段，反序列化(更新)可不传

class RolesSerializer(serializers.ModelSerializer):
    """
    角色序列化类
    """
    permissions = PermissionsSerializer(many=True, read_only=True)  # 一级菜单只允许只读

    class Meta:
        model = Roles
        fields = "__all__"
        read_only_fields = ("id",)   # 仅用于序列化（只读）字段，反序列化(更新)可不传

class UserSerializer(serializers.ModelSerializer):
    """
    用户管理序列化类
    """
    role = RolesSerializer(read_only=True)  # 用户只允许只读
    class Meta:
        model = User
        fields = "__all__"
        read_only_fields = ("id",)   # 仅用于序列化（只读）字段，反序列化(更新)可不传