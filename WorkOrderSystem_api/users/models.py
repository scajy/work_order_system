from django.db import models


class Permissions(models.Model):
    """权限表"""
    name = models.CharField(verbose_name="名称", max_length=64)
    url_path = models.CharField(verbose_name="url路径", max_length=128)
    request = models.CharField(verbose_name="请求方法", max_length=64)
    update_time = models.DateTimeField(verbose_name="更新时间", auto_now=True)
    create_time = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)

    class Meta:
        db_table = "permissions"
        verbose_name_plural = "权限管理"
        ordering = ('id',)

    def __str__(self):
        return self.name

class Roles(models.Model):
    """ 角色表 """
    role_choices = (
        ("admin", "管理员"),
        ("manage", "经理"),
        ("user", "普通用户"),
    )
    role = models.CharField(verbose_name="角色名称", max_length=64, choices=role_choices, default='user')
    name = models.CharField(verbose_name="描述", null=False, blank=False, max_length=50)
    permissions = models.ManyToManyField(Permissions, verbose_name="权限表", blank=True,)  # 多对多
    update_time = models.DateTimeField(verbose_name="更新时间", auto_now=True)
    create_time = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)

    class Meta:
        db_table = "role"
        verbose_name_plural = "角色表"
        ordering = ('id',)

    def __str__(self):
        return self.role

class User(models.Model):
    """用户表"""
    role = models.ForeignKey(Roles, on_delete=models.PROTECT, verbose_name="权限管理", blank=True, null=True, )  # 一对多
    username = models.CharField(verbose_name="登陆账号", max_length=30)
    password = models.CharField(verbose_name="登陆密码", max_length=200)
    name = models.CharField(verbose_name="用户名", max_length=50)
    status = models.CharField(verbose_name="用户状态", max_length=50, null=False, blank=False, choices=(("禁用", "禁用"), ("启用", "启用")), default='禁用')
    department = models.CharField(verbose_name="部门", max_length=50, blank=True, null=True, default=None)
    position = models.CharField(verbose_name="职位", max_length=50, blank=True, null=True, default=None)
    mobile = models.CharField(verbose_name="手机号码", max_length=50)
    email = models.CharField(verbose_name="邮箱", max_length=50)
    last_login = models.DateTimeField(verbose_name="上次登陆时间", auto_now=True)
    create_time = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)
    update_time = models.DateTimeField(verbose_name="修改时间", auto_now=True)

    class Meta:
        db_table = "user"
        verbose_name_plural = "用户表"
        ordering = ('id',)

    def __str__(self):
        return self.name

class Token(models.Model):
    """token认证表 """
    token_key = models.CharField(verbose_name="token", max_length=256)  # token
    created = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)
    User = models.OneToOneField(to=User, on_delete=models.CASCADE)  # 一对一  on_delete=models.CASCADE是级联删除，主表删除后从表也删除

    class Meta:
        db_table = "token"
        verbose_name_plural = "认证表"
        ordering = ('id',)

    def __str__(self):
        return self.token_key
