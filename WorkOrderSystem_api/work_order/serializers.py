# 导入数据模型
from .models import Department, Project, Server, WorkOrder, MasterVersion

# 导入虚拟化工具
from rest_framework import serializers

class DepartmentSerializer(serializers.ModelSerializer):
    """
    部门管理序列化类
    """
    class Meta:
        model = Department
        fields = "__all__"
        read_only_fields = ("id",)   # 仅用于序列化（只读）字段，反序列化(更新)可不传

class ProjectSerializer(serializers.ModelSerializer):
    """
     项目序列化类
    """
    class Meta:
        model = Project
        fields = "__all__"
        read_only_fields = ("id",)   # 仅用于序列化（只读）字段，反序列化(更新)可不传


class ServerSerializer(serializers.ModelSerializer):
    """
    服务模块序列化类
    """
    class Meta:
        model = Server
        fields = "__all__"
        read_only_fields = ("id",)   # 仅用于序列化（只读）字段，反序列化(更新)可不传

class MasterVersionSerializer(serializers.ModelSerializer):
    """
    服务模块序列化类
    """
    class Meta:
        model = MasterVersion
        fields = "__all__"
        read_only_fields = ("id",)   # 仅用于序列化（只读）字段，反序列化(更新)可不传

class WorkOrderSerializer(serializers.ModelSerializer):
    """
    工单序列化类
    """
    department = DepartmentSerializer(read_only=True)  # 部门只允许只读
    project = ProjectSerializer(read_only=True)  # 项目只允许只读
    server = ServerSerializer(read_only=True)  # 服务模块只允许只读
    master_version = MasterVersionSerializer(read_only=True)  # 服务模块只允许只读

    class Meta:
        model = WorkOrder
        fields = "__all__"
        read_only_fields = ("id",)   # 仅用于序列化（只读）字段，反序列化(更新)可不传


