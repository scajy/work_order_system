from django.db import models

# 部门表
class Department(models.Model):
    name = models.CharField(max_length=60, verbose_name="部门名称")
    note = models.TextField(null=True, blank=True, verbose_name="备注")
    create_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    update_time = models.DateTimeField(auto_now=True, verbose_name="创建时间")

    class Meta:
        db_table = 'department'
        verbose_name_plural = '部门管理'
        ordering = ('-id',)

    def __str__(self):
        return self.name

# 项目表
class Project(models.Model):
    name = models.CharField(max_length=60, verbose_name="项目名称")
    note = models.TextField(null=True, blank=True, verbose_name="备注")
    create_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    update_time = models.DateTimeField(auto_now=True, verbose_name="创建时间")

    class Meta:
        db_table = 'project'
        verbose_name_plural = '项目管理'
        ordering = ('-id',)

    def __str__(self):
        return self.name

# 服务表
class Server(models.Model):
    name = models.CharField(max_length=60, verbose_name="服务名称")
    note = models.TextField(null=True, blank=True, verbose_name="备注")
    create_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    update_time = models.DateTimeField(auto_now=True, verbose_name="创建时间")

    class Meta:
        db_table = 'server'
        verbose_name_plural = '服务模块'
        ordering = ('-id',)

    def __str__(self):
        return self.name
# 服务表
class MasterVersion(models.Model):
    name = models.CharField(max_length=60, verbose_name="主版本")
    note = models.TextField(null=True, blank=True, verbose_name="备注")
    create_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    update_time = models.DateTimeField(auto_now=True, verbose_name="创建时间")

    class Meta:
        db_table = 'master-version'
        verbose_name_plural = '主版本'
        ordering = ('-id',)

    def __str__(self):
        return self.name


# 工单表
class WorkOrder(models.Model):
    department = models.ForeignKey(Department, on_delete=models.PROTECT, verbose_name="部门管理")   # 一对多
    project = models.ForeignKey(Project, on_delete=models.PROTECT, verbose_name="项目管理")        # 一对多
    server = models.ForeignKey(Server, on_delete=models.PROTECT, verbose_name="服务模块")  # 一对多
    applicant = models.CharField(max_length=30, verbose_name="申请人")
    work_order_number = models.CharField(max_length=60, verbose_name="工单号")
    fault_description = models.CharField(max_length=240, verbose_name="故障描述")
    Fault_cause = models.TextField(null=True, blank=True, verbose_name="故障原因")
    modify = models.TextField(null=True, blank=True, verbose_name="如何修改")
    reach = models.CharField(max_length=60,  verbose_name="影响范围", choices=(('小', '小'), ('一般','一般'), ('大','大'),('不影响','不影响')),default="不影响")
    master_version = models.ForeignKey(MasterVersion, on_delete=models.PROTECT, verbose_name="主版本号")  #一对多
    version = models.CharField(max_length=120,  verbose_name="子版本号")
    script = models.TextField(null=True, blank=True, verbose_name="脚本")
    config = models.TextField(null=True, blank=True, verbose_name="配置",)
    fallback = models.TextField(null=True, blank=True, verbose_name="回退原因")
    status = models.CharField(max_length=60, verbose_name="状态", choices=(('待分配', '待分配'), ('处理中', '处理中'), ('完成', '完成'),('作废', '作废')), default="待分配")
    priority = models.CharField(max_length=60, verbose_name="优先级", choices=(('低', '低'), ('一般', '一般'), ('高', '高'), ('紧急','紧急')), default="低")
    processed_people = models.CharField(max_length=30, null=True, blank=True, verbose_name="处理人")
    note = models.TextField(null=True, blank=True, verbose_name="备注")
    corresponding_working_hours = models.CharField(null=True, blank=True, max_length=60, verbose_name="响应工时")
    processing_working_hours = models.CharField(null=True, blank=True, max_length=60, verbose_name="处理工时")
    response_time = models.CharField(null=True, blank=True, max_length=60, verbose_name="响应时间")
    processing_time = models.CharField(null=True, blank=True, max_length=60, verbose_name="处理时间")
    create_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    update_time = models.DateTimeField(auto_now=True, null=True, blank=True, verbose_name="修改时间")

    class Meta:
        db_table = 'work_order'
        verbose_name_plural = '工单管理'
        ordering = ('-id',)

    def __str__(self):
        return self.work_order_number
