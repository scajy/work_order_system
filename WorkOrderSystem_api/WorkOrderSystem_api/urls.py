
from django.urls import path, include
from libs import token_auth      # 导入认证登陆接口文件

# 导入视图集
from users.views import UserViewSet, PermissionsViewSet, RolesViewSet
from work_order.views import DepartmentViewSet, ProjectViewSet, ServerViewSet, WorkOrderViewSet, MyWorkOrderViewSet, MasterVersionViewSet, WorkOrderEchartViewSet

# 导入drf路由
from rest_framework import routers
router = routers.DefaultRouter()

# 注册视图集到路由
router.register('user', UserViewSet, basename="user")
router.register('permissions', PermissionsViewSet, basename="permissions")
router.register('roles', RolesViewSet, basename="roles")
router.register('department', DepartmentViewSet, basename="department")
router.register('project', ProjectViewSet, basename="project")
router.register('server', ServerViewSet, basename="server")
router.register('master_version', MasterVersionViewSet, basename="master_version")
router.register('work_order', WorkOrderViewSet, basename="work_order")
router.register('my_work_order', MyWorkOrderViewSet, basename="my_work_order")

urlpatterns = [
    path('api/', include(router.urls)),
    path('login/', token_auth.CustomAuthToken.as_view(), name="login"),   # 配置认证登陆提交路由
    path('change_password/', token_auth.ChangePassword.as_view(), name="change_password"),   # 配置认证登陆提交路由
    path('work_order_echart/', WorkOrderEchartViewSet.as_view(), name="work_order_echart"),   # 配置认证登陆提交路由
]
